puts "Type a year."
year = gets.to_i
puts "Give me a second year, later than the first."
year2 = gets.to_i
puts ""
number_of_leap_yrs = 0
while year <= year2
  if ((year % 100 == 0) && (year % 400 == 0)) || (year % 4 == 0)
    puts year
    number_of_leap_yrs = number_of_leap_yrs + 1
  end
  year += 1
end
if number_of_leap_yrs == 0
  puts "Sorry, I did not find any leap years in that time range."
end