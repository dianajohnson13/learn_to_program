puts "Say something to Grandma."
statement = gets.chomp
while statement != "BYE" 
  if statement == statement.upcase
    puts "NO, NOT SINCE " + rand(1930..1950).to_s + "!"
  else
    puts "Grandma: HUH?! SPEAK UP, SONNY!"
  end
  statement = gets.chomp
end
puts "Grandma: BYE! IT WAS NICE TALKING TO YOU!"
