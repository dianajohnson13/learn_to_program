header = "Table of Contents".center(50)
chapter1 = "Chapter 1: Getting Started".ljust(30) + "page 1".rjust(20)
chapter2 = "Chapter 2: Numbers".ljust(30) + "page 9".rjust(20)
chapter3 = "Chapter 3: Letters".ljust(30) + "page 13".rjust(20)

table_of_contents = [header, chapter1, chapter2, chapter3]


table_of_contents.each do |list|
  puts list
end